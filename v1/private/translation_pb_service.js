// package: language.service.v1.private
// file: v1/private/translation.proto

var v1_private_translation_pb = require("../../v1/private/translation_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var TranslationService = (function () {
  function TranslationService() {}
  TranslationService.serviceName = "language.service.v1.private.TranslationService";
  return TranslationService;
}());

TranslationService.GetTranslation = {
  methodName: "GetTranslation",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.GetTranslationRequest,
  responseType: v1_private_translation_pb.GetTranslationResponse
};

TranslationService.CreateTranslation = {
  methodName: "CreateTranslation",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.CreateTranslationRequest,
  responseType: v1_private_translation_pb.CreateTranslationResponse
};

TranslationService.CreateOrUpdateTranslation = {
  methodName: "CreateOrUpdateTranslation",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.CreateOrUpdateTranslationRequest,
  responseType: v1_private_translation_pb.CreateOrUpdateTranslationResponse
};

TranslationService.DeleteTranslation = {
  methodName: "DeleteTranslation",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.DeleteTranslationRequest,
  responseType: google_protobuf_empty_pb.Empty
};

TranslationService.ListTranslations = {
  methodName: "ListTranslations",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.ListTranslationsRequest,
  responseType: v1_private_translation_pb.ListTranslationsResponse
};

TranslationService.DeleteTranslationLocale = {
  methodName: "DeleteTranslationLocale",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.DeleteTranslationLocaleRequest,
  responseType: google_protobuf_empty_pb.Empty
};

TranslationService.ImportTranslations = {
  methodName: "ImportTranslations",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.ImportTranslationsRequest,
  responseType: v1_private_translation_pb.ImportTranslationsResponse
};

TranslationService.ExportTranslations = {
  methodName: "ExportTranslations",
  service: TranslationService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_translation_pb.ExportTranslationsRequest,
  responseType: v1_private_translation_pb.ExportTranslationsResponse
};

exports.TranslationService = TranslationService;

function TranslationServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

TranslationServiceClient.prototype.getTranslation = function getTranslation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.GetTranslation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.createTranslation = function createTranslation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.CreateTranslation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.createOrUpdateTranslation = function createOrUpdateTranslation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.CreateOrUpdateTranslation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.deleteTranslation = function deleteTranslation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.DeleteTranslation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.listTranslations = function listTranslations(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.ListTranslations, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.deleteTranslationLocale = function deleteTranslationLocale(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.DeleteTranslationLocale, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.importTranslations = function importTranslations(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.ImportTranslations, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TranslationServiceClient.prototype.exportTranslations = function exportTranslations(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TranslationService.ExportTranslations, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.TranslationServiceClient = TranslationServiceClient;

