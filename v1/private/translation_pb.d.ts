// package: language.service.v1.private
// file: v1/private/translation.proto

import * as jspb from "google-protobuf";
import * as v1_common_common_pb from "../../v1/common/common_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class GetTranslationRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTranslationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTranslationRequest): GetTranslationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTranslationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTranslationRequest;
  static deserializeBinaryFromReader(message: GetTranslationRequest, reader: jspb.BinaryReader): GetTranslationRequest;
}

export namespace GetTranslationRequest {
  export type AsObject = {
    namespace: string,
    key: string,
  }
}

export class GetTranslationResponse extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getLocaleToValueMap(): jspb.Map<string, LocaleToValue>;
  clearLocaleToValueMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTranslationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTranslationResponse): GetTranslationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTranslationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTranslationResponse;
  static deserializeBinaryFromReader(message: GetTranslationResponse, reader: jspb.BinaryReader): GetTranslationResponse;
}

export namespace GetTranslationResponse {
  export type AsObject = {
    namespace: string,
    key: string,
    localeToValueMap: Array<[string, LocaleToValue.AsObject]>,
  }
}

export class LocaleToValue extends jspb.Message {
  getValue(): string;
  setValue(value: string): void;

  getOriginal(): string;
  setOriginal(value: string): void;

  hasCreateTime(): boolean;
  clearCreateTime(): void;
  getCreateTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreateTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdateTime(): boolean;
  clearUpdateTime(): void;
  getUpdateTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdateTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdateBy(): string;
  setUpdateBy(value: string): void;

  getCreateBy(): string;
  setCreateBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocaleToValue.AsObject;
  static toObject(includeInstance: boolean, msg: LocaleToValue): LocaleToValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LocaleToValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocaleToValue;
  static deserializeBinaryFromReader(message: LocaleToValue, reader: jspb.BinaryReader): LocaleToValue;
}

export namespace LocaleToValue {
  export type AsObject = {
    value: string,
    original: string,
    createTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updateTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updateBy: string,
    createBy: string,
  }
}

export class CreateTranslationRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getDefaultValue(): string;
  setDefaultValue(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateTranslationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateTranslationRequest): CreateTranslationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateTranslationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateTranslationRequest;
  static deserializeBinaryFromReader(message: CreateTranslationRequest, reader: jspb.BinaryReader): CreateTranslationRequest;
}

export namespace CreateTranslationRequest {
  export type AsObject = {
    namespace: string,
    key: string,
    defaultValue: string,
  }
}

export class CreateTranslationResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateTranslationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateTranslationResponse): CreateTranslationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateTranslationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateTranslationResponse;
  static deserializeBinaryFromReader(message: CreateTranslationResponse, reader: jspb.BinaryReader): CreateTranslationResponse;
}

export namespace CreateTranslationResponse {
  export type AsObject = {
  }
}

export class CreateOrUpdateTranslationRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getDefaultValue(): string;
  setDefaultValue(value: string): void;

  getLocaleToValueMap(): jspb.Map<string, string>;
  clearLocaleToValueMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateTranslationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateTranslationRequest): CreateOrUpdateTranslationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateOrUpdateTranslationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateTranslationRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateTranslationRequest, reader: jspb.BinaryReader): CreateOrUpdateTranslationRequest;
}

export namespace CreateOrUpdateTranslationRequest {
  export type AsObject = {
    namespace: string,
    key: string,
    defaultValue: string,
    localeToValueMap: Array<[string, string]>,
  }
}

export class CreateOrUpdateTranslationResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateTranslationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateTranslationResponse): CreateOrUpdateTranslationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateOrUpdateTranslationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateTranslationResponse;
  static deserializeBinaryFromReader(message: CreateOrUpdateTranslationResponse, reader: jspb.BinaryReader): CreateOrUpdateTranslationResponse;
}

export namespace CreateOrUpdateTranslationResponse {
  export type AsObject = {
  }
}

export class DeleteTranslationRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteTranslationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteTranslationRequest): DeleteTranslationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteTranslationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteTranslationRequest;
  static deserializeBinaryFromReader(message: DeleteTranslationRequest, reader: jspb.BinaryReader): DeleteTranslationRequest;
}

export namespace DeleteTranslationRequest {
  export type AsObject = {
    namespace: string,
    key: string,
  }
}

export class ListTranslationsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): TranslationFilter | undefined;
  setFilter(value?: TranslationFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_common_common_pb.Sort>;
  setSortingsList(value: Array<v1_common_common_pb.Sort>): void;
  addSortings(value?: v1_common_common_pb.Sort, index?: number): v1_common_common_pb.Sort;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_common_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTranslationsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListTranslationsRequest): ListTranslationsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTranslationsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTranslationsRequest;
  static deserializeBinaryFromReader(message: ListTranslationsRequest, reader: jspb.BinaryReader): ListTranslationsRequest;
}

export namespace ListTranslationsRequest {
  export type AsObject = {
    filter?: TranslationFilter.AsObject,
    sortingsList: Array<v1_common_common_pb.Sort.AsObject>,
    pagination?: v1_common_common_pb.PaginationRequest.AsObject,
  }
}

export class TranslationFilter extends jspb.Message {
  clearNamespaceList(): void;
  getNamespaceList(): Array<string>;
  setNamespaceList(value: Array<string>): void;
  addNamespace(value: string, index?: number): string;

  clearLocalesList(): void;
  getLocalesList(): Array<string>;
  setLocalesList(value: Array<string>): void;
  addLocales(value: string, index?: number): string;

  hasKey(): boolean;
  clearKey(): void;
  getKey(): google_protobuf_wrappers_pb.StringValue | undefined;
  setKey(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): google_protobuf_wrappers_pb.StringValue | undefined;
  setValue(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreateTime(): boolean;
  clearCreateTime(): void;
  getCreateTime(): v1_common_common_pb.TimeRange | undefined;
  setCreateTime(value?: v1_common_common_pb.TimeRange): void;

  hasUpdateTime(): boolean;
  clearUpdateTime(): void;
  getUpdateTime(): v1_common_common_pb.TimeRange | undefined;
  setUpdateTime(value?: v1_common_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TranslationFilter.AsObject;
  static toObject(includeInstance: boolean, msg: TranslationFilter): TranslationFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TranslationFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TranslationFilter;
  static deserializeBinaryFromReader(message: TranslationFilter, reader: jspb.BinaryReader): TranslationFilter;
}

export namespace TranslationFilter {
  export type AsObject = {
    namespaceList: Array<string>,
    localesList: Array<string>,
    key?: google_protobuf_wrappers_pb.StringValue.AsObject,
    value?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createTime?: v1_common_common_pb.TimeRange.AsObject,
    updateTime?: v1_common_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListTranslationsResponse extends jspb.Message {
  clearTranslationsList(): void;
  getTranslationsList(): Array<GetTranslationResponse>;
  setTranslationsList(value: Array<GetTranslationResponse>): void;
  addTranslations(value?: GetTranslationResponse, index?: number): GetTranslationResponse;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_common_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTranslationsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListTranslationsResponse): ListTranslationsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTranslationsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTranslationsResponse;
  static deserializeBinaryFromReader(message: ListTranslationsResponse, reader: jspb.BinaryReader): ListTranslationsResponse;
}

export namespace ListTranslationsResponse {
  export type AsObject = {
    translationsList: Array<GetTranslationResponse.AsObject>,
    pagination?: v1_common_common_pb.PaginationResponse.AsObject,
  }
}

export class DeleteTranslationLocaleRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getLocale(): string;
  setLocale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteTranslationLocaleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteTranslationLocaleRequest): DeleteTranslationLocaleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteTranslationLocaleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteTranslationLocaleRequest;
  static deserializeBinaryFromReader(message: DeleteTranslationLocaleRequest, reader: jspb.BinaryReader): DeleteTranslationLocaleRequest;
}

export namespace DeleteTranslationLocaleRequest {
  export type AsObject = {
    namespace: string,
    key: string,
    locale: string,
  }
}

export class ImportTranslationsRequest extends jspb.Message {
  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImportTranslationsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ImportTranslationsRequest): ImportTranslationsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ImportTranslationsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImportTranslationsRequest;
  static deserializeBinaryFromReader(message: ImportTranslationsRequest, reader: jspb.BinaryReader): ImportTranslationsRequest;
}

export namespace ImportTranslationsRequest {
  export type AsObject = {
    data: string,
  }
}

export class ImportTranslationsResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImportTranslationsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ImportTranslationsResponse): ImportTranslationsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ImportTranslationsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImportTranslationsResponse;
  static deserializeBinaryFromReader(message: ImportTranslationsResponse, reader: jspb.BinaryReader): ImportTranslationsResponse;
}

export namespace ImportTranslationsResponse {
  export type AsObject = {
  }
}

export class ExportTranslationsRequest extends jspb.Message {
  clearLocaleList(): void;
  getLocaleList(): Array<string>;
  setLocaleList(value: Array<string>): void;
  addLocale(value: string, index?: number): string;

  clearNamespacesList(): void;
  getNamespacesList(): Array<string>;
  setNamespacesList(value: Array<string>): void;
  addNamespaces(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExportTranslationsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExportTranslationsRequest): ExportTranslationsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ExportTranslationsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExportTranslationsRequest;
  static deserializeBinaryFromReader(message: ExportTranslationsRequest, reader: jspb.BinaryReader): ExportTranslationsRequest;
}

export namespace ExportTranslationsRequest {
  export type AsObject = {
    localeList: Array<string>,
    namespacesList: Array<string>,
  }
}

export class ExportTranslationsResponse extends jspb.Message {
  getContentType(): string;
  setContentType(value: string): void;

  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExportTranslationsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ExportTranslationsResponse): ExportTranslationsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ExportTranslationsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExportTranslationsResponse;
  static deserializeBinaryFromReader(message: ExportTranslationsResponse, reader: jspb.BinaryReader): ExportTranslationsResponse;
}

export namespace ExportTranslationsResponse {
  export type AsObject = {
    contentType: string,
    data: string,
  }
}

