// package: language.service.v1.private
// file: v1/private/namespace.proto

import * as v1_private_namespace_pb from "../../v1/private/namespace_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type NamespaceServiceGetNamespace = {
  readonly methodName: string;
  readonly service: typeof NamespaceService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_namespace_pb.GetNamespaceRequest;
  readonly responseType: typeof v1_private_namespace_pb.GetNamespaceResponse;
};

type NamespaceServiceCreateNamespace = {
  readonly methodName: string;
  readonly service: typeof NamespaceService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_namespace_pb.CreateNamespaceRequest;
  readonly responseType: typeof v1_private_namespace_pb.CreateNamespaceResponse;
};

type NamespaceServiceDeleteNamespace = {
  readonly methodName: string;
  readonly service: typeof NamespaceService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_namespace_pb.DeleteNamespaceRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type NamespaceServiceListNamespaces = {
  readonly methodName: string;
  readonly service: typeof NamespaceService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_namespace_pb.ListNamespacesRequest;
  readonly responseType: typeof v1_private_namespace_pb.ListNamespacesResponse;
};

export class NamespaceService {
  static readonly serviceName: string;
  static readonly GetNamespace: NamespaceServiceGetNamespace;
  static readonly CreateNamespace: NamespaceServiceCreateNamespace;
  static readonly DeleteNamespace: NamespaceServiceDeleteNamespace;
  static readonly ListNamespaces: NamespaceServiceListNamespaces;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class NamespaceServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getNamespace(
    requestMessage: v1_private_namespace_pb.GetNamespaceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.GetNamespaceResponse|null) => void
  ): UnaryResponse;
  getNamespace(
    requestMessage: v1_private_namespace_pb.GetNamespaceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.GetNamespaceResponse|null) => void
  ): UnaryResponse;
  createNamespace(
    requestMessage: v1_private_namespace_pb.CreateNamespaceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.CreateNamespaceResponse|null) => void
  ): UnaryResponse;
  createNamespace(
    requestMessage: v1_private_namespace_pb.CreateNamespaceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.CreateNamespaceResponse|null) => void
  ): UnaryResponse;
  deleteNamespace(
    requestMessage: v1_private_namespace_pb.DeleteNamespaceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteNamespace(
    requestMessage: v1_private_namespace_pb.DeleteNamespaceRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  listNamespaces(
    requestMessage: v1_private_namespace_pb.ListNamespacesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.ListNamespacesResponse|null) => void
  ): UnaryResponse;
  listNamespaces(
    requestMessage: v1_private_namespace_pb.ListNamespacesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_namespace_pb.ListNamespacesResponse|null) => void
  ): UnaryResponse;
}

