// package: language.service.v1.private
// file: v1/private/locale.proto

import * as jspb from "google-protobuf";
import * as v1_common_common_pb from "../../v1/common/common_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";

export class GetLocaleRequest extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetLocaleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetLocaleRequest): GetLocaleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetLocaleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetLocaleRequest;
  static deserializeBinaryFromReader(message: GetLocaleRequest, reader: jspb.BinaryReader): GetLocaleRequest;
}

export namespace GetLocaleRequest {
  export type AsObject = {
    locale: string,
  }
}

export class GetLocaleResponse extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  getActive(): boolean;
  setActive(value: boolean): void;

  getDefault(): boolean;
  setDefault(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetLocaleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetLocaleResponse): GetLocaleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetLocaleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetLocaleResponse;
  static deserializeBinaryFromReader(message: GetLocaleResponse, reader: jspb.BinaryReader): GetLocaleResponse;
}

export namespace GetLocaleResponse {
  export type AsObject = {
    locale: string,
    active: boolean,
    pb_default: boolean,
  }
}

export class CreateLocaleRequest extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  getActive(): boolean;
  setActive(value: boolean): void;

  getDefault(): boolean;
  setDefault(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLocaleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLocaleRequest): CreateLocaleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateLocaleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLocaleRequest;
  static deserializeBinaryFromReader(message: CreateLocaleRequest, reader: jspb.BinaryReader): CreateLocaleRequest;
}

export namespace CreateLocaleRequest {
  export type AsObject = {
    locale: string,
    active: boolean,
    pb_default: boolean,
  }
}

export class CreateLocaleResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLocaleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLocaleResponse): CreateLocaleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateLocaleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLocaleResponse;
  static deserializeBinaryFromReader(message: CreateLocaleResponse, reader: jspb.BinaryReader): CreateLocaleResponse;
}

export namespace CreateLocaleResponse {
  export type AsObject = {
  }
}

export class UpdateLocaleRequest extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  getActive(): boolean;
  setActive(value: boolean): void;

  getDefault(): boolean;
  setDefault(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateLocaleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateLocaleRequest): UpdateLocaleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateLocaleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateLocaleRequest;
  static deserializeBinaryFromReader(message: UpdateLocaleRequest, reader: jspb.BinaryReader): UpdateLocaleRequest;
}

export namespace UpdateLocaleRequest {
  export type AsObject = {
    locale: string,
    active: boolean,
    pb_default: boolean,
  }
}

export class UpdateLocaleResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateLocaleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateLocaleResponse): UpdateLocaleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateLocaleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateLocaleResponse;
  static deserializeBinaryFromReader(message: UpdateLocaleResponse, reader: jspb.BinaryReader): UpdateLocaleResponse;
}

export namespace UpdateLocaleResponse {
  export type AsObject = {
  }
}

export class DeleteLocaleRequest extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteLocaleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteLocaleRequest): DeleteLocaleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteLocaleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteLocaleRequest;
  static deserializeBinaryFromReader(message: DeleteLocaleRequest, reader: jspb.BinaryReader): DeleteLocaleRequest;
}

export namespace DeleteLocaleRequest {
  export type AsObject = {
    locale: string,
  }
}

export class ListLocalesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): LocaleFilter | undefined;
  setFilter(value?: LocaleFilter): void;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_common_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListLocalesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListLocalesRequest): ListLocalesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListLocalesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListLocalesRequest;
  static deserializeBinaryFromReader(message: ListLocalesRequest, reader: jspb.BinaryReader): ListLocalesRequest;
}

export namespace ListLocalesRequest {
  export type AsObject = {
    filter?: LocaleFilter.AsObject,
    pagination?: v1_common_common_pb.PaginationRequest.AsObject,
  }
}

export class ListLocalesResponse extends jspb.Message {
  clearLocalesList(): void;
  getLocalesList(): Array<GetLocaleResponse>;
  setLocalesList(value: Array<GetLocaleResponse>): void;
  addLocales(value?: GetLocaleResponse, index?: number): GetLocaleResponse;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_common_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListLocalesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListLocalesResponse): ListLocalesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListLocalesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListLocalesResponse;
  static deserializeBinaryFromReader(message: ListLocalesResponse, reader: jspb.BinaryReader): ListLocalesResponse;
}

export namespace ListLocalesResponse {
  export type AsObject = {
    localesList: Array<GetLocaleResponse.AsObject>,
    pagination?: v1_common_common_pb.PaginationResponse.AsObject,
  }
}

export class GetDefaultRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDefaultRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDefaultRequest): GetDefaultRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDefaultRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDefaultRequest;
  static deserializeBinaryFromReader(message: GetDefaultRequest, reader: jspb.BinaryReader): GetDefaultRequest;
}

export namespace GetDefaultRequest {
  export type AsObject = {
  }
}

export class GetDefaultResponse extends jspb.Message {
  getLocale(): string;
  setLocale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDefaultResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDefaultResponse): GetDefaultResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDefaultResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDefaultResponse;
  static deserializeBinaryFromReader(message: GetDefaultResponse, reader: jspb.BinaryReader): GetDefaultResponse;
}

export namespace GetDefaultResponse {
  export type AsObject = {
    locale: string,
  }
}

export class LocaleFilter extends jspb.Message {
  hasActive(): boolean;
  clearActive(): void;
  getActive(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setActive(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocaleFilter.AsObject;
  static toObject(includeInstance: boolean, msg: LocaleFilter): LocaleFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LocaleFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocaleFilter;
  static deserializeBinaryFromReader(message: LocaleFilter, reader: jspb.BinaryReader): LocaleFilter;
}

export namespace LocaleFilter {
  export type AsObject = {
    active?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

