// package: language.service.v1.private
// file: v1/private/namespace.proto

var v1_private_namespace_pb = require("../../v1/private/namespace_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var NamespaceService = (function () {
  function NamespaceService() {}
  NamespaceService.serviceName = "language.service.v1.private.NamespaceService";
  return NamespaceService;
}());

NamespaceService.GetNamespace = {
  methodName: "GetNamespace",
  service: NamespaceService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_namespace_pb.GetNamespaceRequest,
  responseType: v1_private_namespace_pb.GetNamespaceResponse
};

NamespaceService.CreateNamespace = {
  methodName: "CreateNamespace",
  service: NamespaceService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_namespace_pb.CreateNamespaceRequest,
  responseType: v1_private_namespace_pb.CreateNamespaceResponse
};

NamespaceService.DeleteNamespace = {
  methodName: "DeleteNamespace",
  service: NamespaceService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_namespace_pb.DeleteNamespaceRequest,
  responseType: google_protobuf_empty_pb.Empty
};

NamespaceService.ListNamespaces = {
  methodName: "ListNamespaces",
  service: NamespaceService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_namespace_pb.ListNamespacesRequest,
  responseType: v1_private_namespace_pb.ListNamespacesResponse
};

exports.NamespaceService = NamespaceService;

function NamespaceServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

NamespaceServiceClient.prototype.getNamespace = function getNamespace(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(NamespaceService.GetNamespace, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

NamespaceServiceClient.prototype.createNamespace = function createNamespace(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(NamespaceService.CreateNamespace, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

NamespaceServiceClient.prototype.deleteNamespace = function deleteNamespace(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(NamespaceService.DeleteNamespace, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

NamespaceServiceClient.prototype.listNamespaces = function listNamespaces(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(NamespaceService.ListNamespaces, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.NamespaceServiceClient = NamespaceServiceClient;

