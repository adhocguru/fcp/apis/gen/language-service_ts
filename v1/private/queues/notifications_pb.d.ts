// package: language.service.v1.private.queues
// file: v1/private/queues/notifications.proto

import * as jspb from "google-protobuf";

export class TranslationValueChanged extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  getKey(): string;
  setKey(value: string): void;

  getLocale(): string;
  setLocale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TranslationValueChanged.AsObject;
  static toObject(includeInstance: boolean, msg: TranslationValueChanged): TranslationValueChanged.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TranslationValueChanged, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TranslationValueChanged;
  static deserializeBinaryFromReader(message: TranslationValueChanged, reader: jspb.BinaryReader): TranslationValueChanged;
}

export namespace TranslationValueChanged {
  export type AsObject = {
    namespace: string,
    key: string,
    locale: string,
  }
}

