// package: language.service.v1.private
// file: v1/private/namespace.proto

import * as jspb from "google-protobuf";
import * as v1_common_common_pb from "../../v1/common/common_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";

export class GetNamespaceRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNamespaceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetNamespaceRequest): GetNamespaceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetNamespaceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNamespaceRequest;
  static deserializeBinaryFromReader(message: GetNamespaceRequest, reader: jspb.BinaryReader): GetNamespaceRequest;
}

export namespace GetNamespaceRequest {
  export type AsObject = {
    namespace: string,
  }
}

export class GetNamespaceResponse extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNamespaceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetNamespaceResponse): GetNamespaceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetNamespaceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNamespaceResponse;
  static deserializeBinaryFromReader(message: GetNamespaceResponse, reader: jspb.BinaryReader): GetNamespaceResponse;
}

export namespace GetNamespaceResponse {
  export type AsObject = {
    namespace: string,
  }
}

export class CreateNamespaceRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateNamespaceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateNamespaceRequest): CreateNamespaceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateNamespaceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateNamespaceRequest;
  static deserializeBinaryFromReader(message: CreateNamespaceRequest, reader: jspb.BinaryReader): CreateNamespaceRequest;
}

export namespace CreateNamespaceRequest {
  export type AsObject = {
    namespace: string,
  }
}

export class CreateNamespaceResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateNamespaceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateNamespaceResponse): CreateNamespaceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateNamespaceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateNamespaceResponse;
  static deserializeBinaryFromReader(message: CreateNamespaceResponse, reader: jspb.BinaryReader): CreateNamespaceResponse;
}

export namespace CreateNamespaceResponse {
  export type AsObject = {
  }
}

export class DeleteNamespaceRequest extends jspb.Message {
  getNamespace(): string;
  setNamespace(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteNamespaceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteNamespaceRequest): DeleteNamespaceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteNamespaceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteNamespaceRequest;
  static deserializeBinaryFromReader(message: DeleteNamespaceRequest, reader: jspb.BinaryReader): DeleteNamespaceRequest;
}

export namespace DeleteNamespaceRequest {
  export type AsObject = {
    namespace: string,
  }
}

export class ListNamespacesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): NamespaceFilter | undefined;
  setFilter(value?: NamespaceFilter): void;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_common_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListNamespacesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListNamespacesRequest): ListNamespacesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListNamespacesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListNamespacesRequest;
  static deserializeBinaryFromReader(message: ListNamespacesRequest, reader: jspb.BinaryReader): ListNamespacesRequest;
}

export namespace ListNamespacesRequest {
  export type AsObject = {
    filter?: NamespaceFilter.AsObject,
    pagination?: v1_common_common_pb.PaginationRequest.AsObject,
  }
}

export class ListNamespacesResponse extends jspb.Message {
  clearNamespacesList(): void;
  getNamespacesList(): Array<GetNamespaceResponse>;
  setNamespacesList(value: Array<GetNamespaceResponse>): void;
  addNamespaces(value?: GetNamespaceResponse, index?: number): GetNamespaceResponse;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_common_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListNamespacesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListNamespacesResponse): ListNamespacesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListNamespacesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListNamespacesResponse;
  static deserializeBinaryFromReader(message: ListNamespacesResponse, reader: jspb.BinaryReader): ListNamespacesResponse;
}

export namespace ListNamespacesResponse {
  export type AsObject = {
    namespacesList: Array<GetNamespaceResponse.AsObject>,
    pagination?: v1_common_common_pb.PaginationResponse.AsObject,
  }
}

export class NamespaceFilter extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NamespaceFilter.AsObject;
  static toObject(includeInstance: boolean, msg: NamespaceFilter): NamespaceFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NamespaceFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NamespaceFilter;
  static deserializeBinaryFromReader(message: NamespaceFilter, reader: jspb.BinaryReader): NamespaceFilter;
}

export namespace NamespaceFilter {
  export type AsObject = {
  }
}

