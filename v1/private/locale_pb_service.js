// package: language.service.v1.private
// file: v1/private/locale.proto

var v1_private_locale_pb = require("../../v1/private/locale_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var LocaleService = (function () {
  function LocaleService() {}
  LocaleService.serviceName = "language.service.v1.private.LocaleService";
  return LocaleService;
}());

LocaleService.GetLocale = {
  methodName: "GetLocale",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.GetLocaleRequest,
  responseType: v1_private_locale_pb.GetLocaleResponse
};

LocaleService.CreateLocale = {
  methodName: "CreateLocale",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.CreateLocaleRequest,
  responseType: v1_private_locale_pb.CreateLocaleResponse
};

LocaleService.UpdateLocale = {
  methodName: "UpdateLocale",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.UpdateLocaleRequest,
  responseType: v1_private_locale_pb.UpdateLocaleResponse
};

LocaleService.DeleteLocale = {
  methodName: "DeleteLocale",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.DeleteLocaleRequest,
  responseType: google_protobuf_empty_pb.Empty
};

LocaleService.ListLocales = {
  methodName: "ListLocales",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.ListLocalesRequest,
  responseType: v1_private_locale_pb.ListLocalesResponse
};

LocaleService.GetDefault = {
  methodName: "GetDefault",
  service: LocaleService,
  requestStream: false,
  responseStream: false,
  requestType: v1_private_locale_pb.GetDefaultRequest,
  responseType: v1_private_locale_pb.GetDefaultResponse
};

exports.LocaleService = LocaleService;

function LocaleServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

LocaleServiceClient.prototype.getLocale = function getLocale(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.GetLocale, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

LocaleServiceClient.prototype.createLocale = function createLocale(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.CreateLocale, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

LocaleServiceClient.prototype.updateLocale = function updateLocale(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.UpdateLocale, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

LocaleServiceClient.prototype.deleteLocale = function deleteLocale(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.DeleteLocale, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

LocaleServiceClient.prototype.listLocales = function listLocales(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.ListLocales, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

LocaleServiceClient.prototype.getDefault = function getDefault(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(LocaleService.GetDefault, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.LocaleServiceClient = LocaleServiceClient;

